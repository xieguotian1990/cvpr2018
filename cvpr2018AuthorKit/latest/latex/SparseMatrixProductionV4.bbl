\begin{thebibliography}{10}\itemsep=-1pt

\bibitem{Chollet16a}
F.~Chollet.
\newblock Xception: Deep learning with depthwise separable convolutions.
\newblock {\em CoRR}, abs/1610.02357, 2016.

\bibitem{cordeirowide}
L.~Cordeiro.
\newblock Wide residual network for the tiny imagenet challenge.

\bibitem{courbariaux2016binarized}
M.~Courbariaux, I.~Hubara, D.~Soudry, R.~El-Yaniv, and Y.~Bengio.
\newblock Binarized neural networks: Training deep neural networks with weights
  and activations constrained to+ 1 or-1.
\newblock {\em arXiv preprint arXiv:1602.02830}, 2016.

\bibitem{DentonZBLF14}
E.~L. Denton, W.~Zaremba, J.~Bruna, Y.~LeCun, and R.~Fergus.
\newblock Exploiting linear structure within convolutional networks for
  efficient evaluation.
\newblock In {\em {NIPS}}, pages 1269--1277, 2014.

\bibitem{HanMD15}
S.~Han, H.~Mao, and W.~J. Dally.
\newblock Deep compression: Compressing deep neural network with pruning,
  trained quantization and huffman coding.
\newblock {\em CoRR}, abs/1510.00149, 2015.

\bibitem{HeZRS16}
K.~He, X.~Zhang, S.~Ren, and J.~Sun.
\newblock Deep residual learning for image recognition.
\newblock In {\em {CVPR}}, pages 770--778, 2016.

\bibitem{he2016deep}
K.~He, X.~Zhang, S.~Ren, and J.~Sun.
\newblock Deep residual learning for image recognition.
\newblock In {\em Proceedings of the IEEE conference on computer vision and
  pattern recognition}, pages 770--778, 2016.

\bibitem{he2017channel}
Y.~He, X.~Zhang, and J.~Sun.
\newblock Channel pruning for accelerating very deep neural networks.
\newblock {\em arXiv preprint arXiv:1707.06168}, 2017.

\bibitem{huang2017snapshot}
G.~Huang, Y.~Li, G.~Pleiss, Z.~Liu, J.~E. Hopcroft, and K.~Q. Weinberger.
\newblock Snapshot ensembles: Train 1, get m for free.
\newblock {\em arXiv preprint arXiv:1704.00109}, 2017.

\bibitem{HuangLW16a}
G.~Huang, Z.~Liu, and K.~Q. Weinberger.
\newblock Densely connected convolutional networks.
\newblock {\em CoRR}, abs/1608.06993, 2016.

\bibitem{huang2016deep}
G.~Huang, Y.~Sun, Z.~Liu, D.~Sedra, and K.~Q. Weinberger.
\newblock Deep networks with stochastic depth.
\newblock In {\em European Conference on Computer Vision}, pages 646--661.
  Springer, 2016.

\bibitem{IoannouRCC16}
Y.~Ioannou, D.~P. Robertson, R.~Cipolla, and A.~Criminisi.
\newblock Deep roots: Improving {CNN} efficiency with hierarchical filter
  groups.
\newblock {\em CoRR}, abs/1605.06489, 2016.

\bibitem{IoannouRSCC15}
Y.~Ioannou, D.~P. Robertson, J.~Shotton, R.~Cipolla, and A.~Criminisi.
\newblock Training cnns with low-rank filters for efficient image
  classification.
\newblock {\em CoRR}, abs/1511.06744, 2015.

\bibitem{JaderbergVZ14}
M.~Jaderberg, A.~Vedaldi, and A.~Zisserman.
\newblock Speeding up convolutional neural networks with low rank expansions.
\newblock In {\em {BMVC}}, 2014.

\bibitem{KimPYCYS15}
Y.~Kim, E.~Park, S.~Yoo, T.~Choi, L.~Yang, and D.~Shin.
\newblock Compression of deep convolutional neural networks for fast and low
  power mobile applications.
\newblock {\em CoRR}, abs/1511.06530, 2015.

\bibitem{krizhevsky2009learning}
A.~Krizhevsky.
\newblock Learning multiple layers of features from tiny images.
\newblock 2009.

\bibitem{Le2015TinyIV}
Y.~Le and X.~Yang.
\newblock Tiny imagenet visual recognition challenge.
\newblock 2015.

\bibitem{li2016ternary}
F.~Li, B.~Zhang, and B.~Liu.
\newblock Ternary weight networks.
\newblock {\em arXiv preprint arXiv:1605.04711}, 2016.

\bibitem{LiKDSG16}
H.~Li, A.~Kadav, I.~Durdanovic, H.~Samet, and H.~P. Graf.
\newblock Pruning filters for efficient convnets.
\newblock {\em CoRR}, abs/1608.08710, 2016.

\bibitem{liu2015sparse}
B.~Liu, M.~Wang, H.~Foroosh, M.~Tappen, and M.~Penksy.
\newblock Sparse convolutional neural networks.
\newblock In {\em Computer Vision and Pattern Recognition (CVPR), 2015 IEEE
  Conference on}, pages 806--814. IEEE, 2015.

\bibitem{MamaletG12}
F.~Mamalet and C.~Garcia.
\newblock Simplifying convnets for fast learning.
\newblock In {\em {ICANN}}, pages 58--65, 2012.

\bibitem{mao2017exploring}
H.~Mao, S.~Han, J.~Pool, W.~Li, X.~Liu, Y.~Wang, and W.~J. Dally.
\newblock Exploring the regularity of sparse structure in convolutional neural
  networks.
\newblock {\em arXiv preprint arXiv:1705.08922}, 2017.

\bibitem{park2016faster}
J.~Park, S.~Li, W.~Wen, P.~T.~P. Tang, H.~Li, Y.~Chen, and P.~Dubey.
\newblock Faster cnns with direct sparse convolutions and guided pruning.
\newblock 2016.

\bibitem{rastegari2016xnor}
M.~Rastegari, V.~Ordonez, J.~Redmon, and A.~Farhadi.
\newblock Xnor-net: Imagenet classification using binary convolutional neural
  networks.
\newblock In {\em European Conference on Computer Vision}, pages 525--542.
  Springer, 2016.

\bibitem{ILSVRC15}
O.~Russakovsky, J.~Deng, H.~Su, J.~Krause, S.~Satheesh, S.~Ma, Z.~Huang,
  A.~Karpathy, A.~Khosla, M.~Bernstein, A.~C. Berg, and L.~Fei-Fei.
\newblock {ImageNet Large Scale Visual Recognition Challenge}.
\newblock {\em International Journal of Computer Vision (IJCV)},
  115(3):211--252, 2015.

\bibitem{singh2016swapout}
S.~Singh, D.~Hoiem, and D.~Forsyth.
\newblock Swapout: Learning an ensemble of deep architectures.
\newblock In {\em Advances in Neural Information Processing Systems}, pages
  28--36, 2016.

\bibitem{WangWZZ16}
J.~Wang, Z.~Wei, T.~Zhang, and W.~Zeng.
\newblock Deeply-fused nets.
\newblock {\em CoRR}, abs/1605.07716, 2016.

\bibitem{WenWWCL16}
W.~Wen, C.~Wu, Y.~Wang, Y.~Chen, and H.~Li.
\newblock Learning structured sparsity in deep neural networks.
\newblock In {\em NIPS}, pages 2074--2082, 2016.

\bibitem{wen2017coordinating}
W.~Wen, C.~Xu, C.~Wu, Y.~Wang, Y.~Chen, and H.~Li.
\newblock Coordinating filters for faster deep neural networks.
\newblock {\em arXiv preprint arXiv:1703.09746}, 2017.

\bibitem{XieGDTH16}
S.~Xie, R.~B. Girshick, P.~Doll{\'{a}}r, Z.~Tu, and K.~He.
\newblock Aggregated residual transformations for deep neural networks.
\newblock {\em CoRR}, abs/1611.05431, 2016.

\bibitem{ZhangQXW17}
T.~Zhang, G.~Qi, B.~Xiao, and J.~Wang.
\newblock Interleaved group convolutions for deep neural networks.
\newblock {\em CoRR}, abs/1707.02725, 2017.

\bibitem{ZhaoWLTZ16}
L.~Zhao, J.~Wang, X.~Li, Z.~Tu, and W.~Zeng.
\newblock On the connection of deep fusion to ensembling.
\newblock {\em CoRR}, abs/1611.07718, 2016.

\bibitem{zhao2016connection}
L.~Zhao, J.~Wang, X.~Li, Z.~Tu, and W.~Zeng.
\newblock On the connection of deep fusion to ensembling.
\newblock {\em arXiv preprint arXiv:1611.07718}, 2016.

\bibitem{zhou2017incremental}
A.~Zhou, A.~Yao, Y.~Guo, L.~Xu, and Y.~Chen.
\newblock Incremental network quantization: Towards lossless cnns with
  low-precision weights.
\newblock {\em arXiv preprint arXiv:1702.03044}, 2017.

\bibitem{zhou2016dorefa}
S.~Zhou, Y.~Wu, Z.~Ni, X.~Zhou, H.~Wen, and Y.~Zou.
\newblock Dorefa-net: Training low bitwidth convolutional neural networks with
  low bitwidth gradients.
\newblock {\em arXiv preprint arXiv:1606.06160}, 2016.

\bibitem{zhu2016trained}
C.~Zhu, S.~Han, H.~Mao, and W.~J. Dally.
\newblock Trained ternary quantization.
\newblock {\em arXiv preprint arXiv:1612.01064}, 2016.

\end{thebibliography}
