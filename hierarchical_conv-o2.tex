\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{diagbox}
\usepackage{multirow}
\usepackage{caption}
\usepackage{subcaption}
% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}

% \cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{****} % *** Enter the CVPR Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifcvprfinal\pagestyle{empty}\fi

\newtheorem{condition}{Condition}


\begin{document}

%%%%%%%%% TITLE
\title{Hierarchical Decomposition of Convolution}

\author{First Author\\
Institution1\\
Institution1 address\\
{\tt\small firstauthor@i1.org}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Second Author\\
Institution2\\
First line of institution2 address\\
{\tt\small secondauthor@i2.org}
}

\maketitle
%\thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}
In this paper, we shows a way to build relationships among all spatial configurations using a slight way, hierarchical structure.  This is a way to build relationships with low cost, both low computation cost and low parameters cost.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}

Deep convolutional neural networks
with small model size, low computation cost,
but still high accuracy
becomes an urgent request
especially in mobile devices.
The efforts include
(i) network compression:
compress the pretrained model
by decomposing the convolutional kernel matrix
or deleting connections or channels
to remove redundancy,
and (ii) architecture design:
design small kernels,
sparse kernels
or use the product of less-redundant kernels
to approach single kernels
and train the networks from scratch.

Our study lies in architecture design
using
the product of less redundant kernels
for kernel approximation.
There are two main ways:
multiply low-rank kernel (matrices)
to approximate a high-rank kernel
e.g., bottleneck modules~\cite{}
and multiply sparse matrices,
which attracted research efforts recently
and is the focus of this paper. ???

We point out that the recently-developed algorithms,
such as
interleaved group convolution~\cite{}, deep roots~\cite{}, and Xception~\cite{},
approximate a dense kernel
using the product of two kernels,
where at least one kernel is structurally (?) sparse (block-diagonal).
We observe that
one of the two kernels can be further approximated.
For example,
the $1 \times 1$ kernel in Xception and deep roots
can be approximated
by the product of two block diagonal matrices.
The suggested secondary group convolution in interleaved group convolution
contains two branches
and each branch is a $1\times 1$ convolution,
which similarly can be further approximated.
This further reduces the redundancy.
In other words,
using the product of more sparse kernels
to approximate a dense kernel
consume fewer parameters.

Motivated by this,
we design a building block
that consists of successive group convolutions.
This block is mathematically
formulated as
multiplying block diagonal matrices,
each of which corresponds to a group convolution.


\section{Our Approach}

\noindent\textbf{Regular convolution.}
In convolutional neural networks,
the convolution operation relies on 
a matrix-vector multiplication operation:
\begin{align} 
\mathbf{y} = \mathbf{W} \mathbf{x}.
\end{align}
Here the input $\mathbf{x}$,
formed from the input channels,
is a $SD_i$-dimensional vector,
with $S$ being the kernel size (e.g., $S= 3 \times 3$),
$D_i$ being the number of input channels. 
The output $\mathbf{y}$
is a $D_o$-dimensional vector,
with $D_o$ being the number of output channels.
$\mathbf{W}$ is formed
from $D_o$ convolutional kernels
and each row corresponds to a convolutional kernel.
In the following, we assume $D_i=D_o = D$,
but all the formulations can be generalized 
to $D_i \neq D_o$.

\subsection{Product of Two Sparse Matrices}
Recent architecture design algorithms,
Xception, deep roots, and interleaved group convolutions,
decomposes $\mathbf{W}$
as a product of two matrices,
\begin{align}
\mathbf{y} = \mathbf{P}^2\mathbf{W}^2 \mathbf{P}^1\mathbf{W}^1 \mathbf{x},
\label{eqn:twosparsematrixproduct}
\end{align}
where at least one matrix, 
$\mathbf{W}^1$ or $\mathbf{W}^2$, 
is a partitioned sparse matrix,
$\mathbf{P}^i$ is a permutation matrix 
that is used to reorder the channels. 
and $\mathbf{P}^2\mathbf{W}^2 \mathbf{P}^1\mathbf{W}^1$ is a dense matrix. 

\vspace{.1cm}
\noindent\textbf{Interleaved group convolution.}
The interleaved group convolution block
consists of primary and secondary group convolutions.
Both $\mathbf{W}^2$ and $\mathbf{W}^1$ can be sparse,
\begin{align}
  \mathbf{W}^i =  \begin{bmatrix}
     \mathbf{W}_{1}^i & \boldsymbol{0} &  \boldsymbol{0} &  \boldsymbol{0} \\[0.3em]
     \boldsymbol{0} & \mathbf{W}_{2}^i  & \boldsymbol{0} & \boldsymbol{0} \\[0.3em]
     \vdots & \vdots & \ddots  & \vdots \\[0.3em]
     \boldsymbol{0} & \boldsymbol{0} & \boldsymbol{0} & \mathbf{W}_{G_i}^i
     \end{bmatrix},
     \label{eqn:groupconvolution}
\end{align}
where $\mathbf{W}_{g}^i$ is
the kernel matrix
over the corresponding channels.
In the suggested case,
$G_2 = 2$, and
$\mathbf{W}^2_1$ and $\mathbf{W}^2_2$
are both dense matrices of size $\frac{d}{2} \times \frac{d}{2}$.
$G_1 = \frac{d}{2}$,
$\mathbf{W}^1_g$ is a matrix of size $S \times 2$.


\vspace{.1cm}
\noindent\textbf{Xception.}
The Xception block consists of
a $1\times 1$ convolution layer
followed by a channel-wise convolution layer.
It is pointed out that the order of the two layers does not make effects.
For convenience, we below discuss the mathematical formulation 
putting the $1 \times 1$ convolution later.
$\mathbf{W}^2$ is a dense matrix of size $d \times d$,
and $\mathbf{W}^1$ is a partitioned sparse matrix of size $d \times (Sd)$:
\begin{align}
  \mathbf{W}^1 =  \begin{bmatrix}
     (\mathbf{w}_{1}^1)^\top & \boldsymbol{0} &  \boldsymbol{0} &  \boldsymbol{0} \\[0.3em]
     \boldsymbol{0} & (\mathbf{w}_{2}^1)^\top  & \boldsymbol{0} & \boldsymbol{0} \\[0.3em]
     \vdots & \vdots & \ddots  & \vdots \\[0.3em]
     \boldsymbol{0} & \boldsymbol{0} & \boldsymbol{0} & (\mathbf{w}_{D}^1)^\top
     \end{bmatrix},
     \label{eqn:channelwiseconvolution}
\end{align}
where $\mathbf{w}^1_d$ ($d=1, \dots, D$)
is a $S$-dimensional vector 
and the kernel for the $d$th channel.



\vspace{.1cm}
\noindent\textbf{Deep roots.}
In deep roots,
$\mathbf{W}^2$ is a dense matrix of size $d \times d$,
i.e., corresponding to a $1 \times 1$ convolution
while $\mathbf{W}^2$ is a partitioned sparse matrix,
i.e., corresponding to a group convolution.

\vspace{.1cm}
\noindent\textbf{Complexity.}
The computation complexity
of Equation~\ref{eqn:twosparsematrixproduct}
is $O(|\mathbf{W}^1|_0 + |\mathbf{W}^2|_0)$,
where $|\mathbf{W}^i|_0$
is the number of non-zero entries.
In the case that the sparse matrices are partitioned sparse matrices 
as given in Equations~\ref{eqn:groupconvolution} and~\ref{eqn:channelwiseconvolution},
the storage/memeory cost is also $O(|\mathbf{W}^1|_0 + |\mathbf{W}^2|_0)$.


\subsection{Product of More Sparse Matrices}
Our approach is motivated by
the observations:
(i) the block $\mathbf{W}_g^i$ in Equation~\ref{eqn:groupconvolution}
and the $1\times 1$ convolution in Xception
are dense and can be further approximated
by the product of sparse matrices,
further saving the storage and time cost;
and (2) 
such a process can be repeated more times.

The mathematical formulation is given as follows,
\begin{align}
\mathbf{y} =~& \mathbf{P}_L\mathbf{W}_L \mathbf{P}_{L-1}\mathbf{W}_{L-1} \dots \mathbf{P}_1\mathbf{W}_1 \mathbf{x} \\
=~& (\prod_{l=L}^1 \mathbf{P}_l \mathbf{W}_l) \mathbf{x}. 
\label{eqn:moresparsematrixproduct}
\end{align}
Here $\{ \mathbf{W}_l\}$
are partitioned sparse matrices as given in Equation~\ref{eqn:groupconvolution},
with each block being of the same size,
and each corresponds to a group convolution.
$\{ \mathbf{P}_l\}$ are permutation matrices.

We follow the design in interleaved group convolution~\cite{}
to construct the $L$ group convolutions
to satisfy the complementary (orthogonality) condition.

\begin{condition}[Orthogonality]
For any $m$,
$(\mathbf{W}_L\prod_{l=L-1}^m \mathbf{P}_l \mathbf{W}_l)$
corresponds to a group convolution
and 
$(\mathbf{W}_{m-1}\prod_{l=m-2}^1 \mathbf{P}_l \mathbf{W}_l)$
also corresponds to a group convolution.
The two group convolutions are orthogonal:
the channels lying in the same partition 
in one group convolution 
lie in different partitions in the other group convolution.
\end{condition}


\noindent\textbf{Nested construction
from Xception and IGC.}
The group convolutions
can be constructed by recursively
building primary and secondary group convolutions
as done in interleaved group convolutions.
For example,
the dense matrix $\mathbf{W}^2$
(corresponding to a $1 \times 1$ convolution)
in Xception
can be approximated by
two partitioned sparse matrices 
(two group convolutions).
In interleaved group convolutions,
each submatrix in Equation~\ref{eqn:groupconvolution}
in the secondary group convolution
corresponds to a (dense) $1 \times 1$ convolution
over a subset of channels,
and thus can be further approximated by two partition sparse matrices:
\begin{align}
\mathbf{W}^2_g
= ~& \mathbf{P}^{22}_g
     \begin{bmatrix}
     \mathbf{W}_{g1}^{22} & \boldsymbol{0} &  \boldsymbol{0} &  \boldsymbol{0} \\[0.3em]
     \boldsymbol{0} & \mathbf{W}_{g2}^{22}  & \boldsymbol{0} & \boldsymbol{0} \\[0.3em]
     \vdots & \vdots & \ddots  & \vdots \\[0.3em]
     \boldsymbol{0} & \boldsymbol{0} & \boldsymbol{0} & \mathbf{W}_{gG_{22}}^{22}
     \end{bmatrix} \\
    & \mathbf{P}^{21}_g
     \begin{bmatrix}
     \mathbf{W}_{g1}^{21} & \boldsymbol{0} &  \boldsymbol{0} &  \boldsymbol{0} \\[0.3em]
     \boldsymbol{0} & \mathbf{W}_{g2}^{21}  & \boldsymbol{0} & \boldsymbol{0} \\[0.3em]
     \vdots & \vdots & \ddots  & \vdots \\[0.3em]
     \boldsymbol{0} & \boldsymbol{0} & \boldsymbol{0} & \mathbf{W}_{gG_{21}}^{21} 
     \end{bmatrix}\\
    = ~& 
    \mathbf{P}^{22}_g
    \mathbf{W}_{g}^{22}
    \mathbf{P}^{21}_g
    \mathbf{W}_{g}^{21},
\end{align}
where the definition of $\mathbf{W}_{gt}^{2j}$ is similar to Equation~\ref{eqn:groupconvolution}.
It can be shown that 
\begin{align}
\mathbf{W}^2
= \mathbf{P}^{22}
\mathbf{W}^{22}
\mathbf{P}^{21}
\mathbf{W}^{21},
\end{align}
and 
\begin{align}
\mathbf{W}^{2i} = 
\begin{bmatrix}
\mathbf{W}_{1}^{2i} & \boldsymbol{0} &  \boldsymbol{0} &  \boldsymbol{0} \\[0.3em]
     \boldsymbol{0} & \mathbf{W}_{2}^{2i}  & \boldsymbol{0} & \boldsymbol{0} \\[0.3em]
     \vdots & \vdots & \ddots  & \vdots \\[0.3em]
     \boldsymbol{0} & \boldsymbol{0} & \boldsymbol{0} & \mathbf{W}_{G_{2}}^{2i}.
\end{bmatrix}
\end{align}


We show that
more sparse matrices,
whose product is used
to approximate a dense matrix,
is able to lead to smaller complexity.
Our analysis is based on
that the structured sparse matrix 
corresponds to group convolutions,
i.e., in the form shown in Equation~\ref{eqn:groupconvolution},
which can be generalized to other sparse matrices.



\section{Experiment}
\subsection{Empirical Study}
\subsection{Empirical Analysis}

\noindent\textbf{More could be better.}
\begin{table}
	\footnotesize
	\begin{center}
		\begin{tabular}{|l|l|c|c|c|}			
			
			\hline
			\multirow{2}{*}{width(D)} & IGC & 64 & 96 & 128 \\			
			& hconv & 98 &162 &242 \\
			\hline
			\multirow{2}{*}{\#params} & IGC &3200&6336 & 10496  \\
			& hconv &3136& 5832 & 9680  \\	
			\hline
			\multirow{2}{*}{cifar100} & IGC &$60.51\pm0.24$&$64.43\pm0.10$&$66.39\pm0.68$\\		
			& hconv &$61.28\pm0.50$&$65.64\pm0.62$&$67.21\pm0.34$\\
			\hline
			\multirow{2}{*}{cifar10} & IGC &-&-&-\\		
			& hconv &-&-&-\\			
			\hline
			\hline
			\multirow{2}{*}{width(D)} & DW & 64 & 96 & 128 \\			
			& hconv & 144 & 256 & 361 \\
			\hline
			\multirow{2}{*}{\#params} & DW &4672&10080 & 17536  \\
			& hconv &4752& 10496 & 16967  \\	
			\hline
			\multirow{2}{*}{cifar100} & DW &$62.81\pm0.53$&$65.18\pm0.37$&$66.51\pm0.68$\\		
			& hconv &$65.046\pm0.18$&$67.90\pm0.18$&$68.86\pm0.57$\\	
			\hline
			\multirow{2}{*}{cifar10} & DW &-&-&-\\		
			& hconv &-&-&-\\						 
			\hline
			
		\end{tabular}
	\end{center}
	\caption{By decomposing the $1\times1$ convolution into 2 matrix, our model with hconv has better performance than IGC and hconv, which shows that more decomposing is better.}
	\label{table-two-partition}
\end{table}
We replace the $1\times 1$ convolution depthwise and IGC with two matrix of hconv. The result is shown in table \ref{table-two-partition}

Comparison with Xception. $1 \times 1 $, $L=2$



Comparison with IGC.


\vspace{.1cm}
\noindent\textbf{The effect of $K$ with $L$ fixed.}
We fixed the number of layer $L$, and changing $K$ in $\{4,6,8,12\}$. The comparison is shown in figure \ref{fig-select-K}. The results shows that, a small $K$ is suggested to have better performance when $L$ is large, and a large $K$ is suggested when $L$ is small.
\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{fig/K_change_L_fixed.pdf}
	\caption{When the number of layer $L$ is large, Select a small $K$ has the better performance. When the number of layer $L$ is small,  select a large $K$ has the better performance.}
	\label{fig-select-K}
\end{figure}

\vspace{.1cm}
\noindent\textbf{The effect of $L$ with optimal $K$.}

See Figure~\ref{fig-select-layer}.
\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{fig/LogKN.pdf}
	\caption{Relation among K, L and N. When $L\approx \log_K(N)$, the network can achieve best performance. }
	\label{fig-logkn}
\end{figure}
\begin{table}
	\footnotesize
	\begin{center}
		\begin{tabular}{|l|l|c|c|c|c|c|c|}			
			\hline
			&L & 1 & 2 & 3 & 4 & 5 & 6 \\
			\hline
			\multirow{2}{*}{$K=4$} &N & 360 & 272 & 220 & 184 & 160 & 140 \\
			& \#params& 4680 & 4624 & 4620 &4600 &4640 & 4620\\
			\hline
			\multirow{2}{*}{$K=6$} & N & 308 & 222 & 174 & 144 & 120 & 102 \\
			&\#params& 4620 & 4662 & 4698 &4752 & 4680 & 4590\\
			\hline
			\multirow{2}{*}{$K=8$}&N & 272 & 184 & 136 & 112 & 88 & 80 \\
			&\#params& 4624 & 4600 & 4488 & 4592 & 4321 & 4560\\
			\hline
			\multirow{2}{*}{$K=12$}& N & 228 & 144 & 108 & 84 & 72 & 60 \\
			&\#params& 4788 & 4752 & 4860 & 4788 & 4968 & 4860\\
			\hline		    		    			
		\end{tabular}
	\end{center}
	\caption{Corresponding width and \#params}
	\label{table-logkn}
\end{table}

\paragraph{Relation among K , L and N.}
We use a plain network with 8 layers for experiments, which is also used in IGC paper ~\cite{}. For each experiment setting, keeping K is fixed, then changing L from 1 to 6, while N is set to keep the number parameters of each layer of the network is nearly the same. The setting is in table \ref{table-logkn}. We plot the curve of accuracy against $\frac{L}{\log_K(N)}$, in Figure \ref{fig-logkn}, From which, we found that when $L\approx \log_K(N)$, the network can achieve best performance. The reason is that (1) when $L < \log_K(N)$, there are many input neurons that are not connecting to the output neuron, so that the output neuron contains few information of the previous layers, causing the expressibility of the output neuron is week. (2) when $L > \log_K(N)$, the width of this layer is small, so that expressibility of this layer is week. In conclusion, setting $L\approx \log_K(N)$ can make balance between the expressibility of neuron and the whole layer.
\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{fig/select_layer.pdf}
	\caption{When the number of layer $L$ increasing, the network is getting wider. However, more number of layer will cause a drop of performance. We suggest to set $L$ in the range $[2,4]$.}
	\label{fig-select-layer}
\end{figure}
\begin{table}
	
	\begin{center}
		\begin{tabular}{|l|c|c|c|c|c|c|}			
			\hline
			L & 1 & 2 & 3 & 4 & 5 &8\\
			\hline
			K & 64 & 12 & 6 &4 &3 &2 \\
			\hline
			N & 64 & 144 & 174 & 184 & 192 & 186 \\
			\hline
			\#params & 4672 & 4752 & 4698 & 4600 & 4608 & 4650\\
			\hline   		    			
		\end{tabular}
	\end{center}
	\caption{Setting L to increase and explore how does it take effect on the performance of the network. }
	\label{table-select-layer}
\end{table}
\paragraph{Effect of The Number of Layer.}
We use the plain network with 8 layers for experiments. By setting L to increase from 1 to 8, we select the K so that the width of one layer is the largest, keeping the relation $L=\log_K(N)$ and the total \#params of this layer is approximately 4672. The setting is shown in table \ref{table-select-layer}. The result is shown in figure \ref{fig-select-layer}, from which we can observe that bad performance when the $L$ is too small and too large. To note that, when $L=1$ in the figure \ref{fig-select-layer}, it's the network with depthwise separable convolution, with the fact that depthwise separable convolution is the specific case of our scheme. When $L$ is small, the width of the network is small, so that the performance is worse. Whe $L$ is large, the hierarchical structure will cause the gradient vanishing, so that it's hard to update the parameters of one layer and the performance is worse. We suggest setting $L$ in range $[2,4]$
\paragraph{From Deeper to Wider Network.}
\begin{figure*}[h!]
	\begin{subfigure}[b]{0.25\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{fig/d2w_param_plain.pdf}
		\caption{plain-\#params}
		\label{fig-d2w-param-plain}
	\end{subfigure}%
	\begin{subfigure}[b]{0.25\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{fig/d2w_flops_plain.pdf}
		\caption{plain-FLOPs}
		\label{fig-d2w-flops-plain}
	\end{subfigure}%
	\begin{subfigure}[b]{0.25\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{fig/d2w_param.pdf}
		\caption{Ident-\#params}
		\label{fig-d2w-param}
	\end{subfigure}%
	\begin{subfigure}[b]{0.25\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{fig/d2w_flops.pdf}
		\caption{Ident-FLOPs}
		\label{fig-d2w-flops}
	\end{subfigure}
	\caption{Pictures of animals}\label{fig:animals}
\end{figure*}
%\begin{figure}
%	\centering
%	\includegraphics[width=0.5\textwidth]{fig/d2w_param.pdf}
%	\caption{ }
%	\label{fig-d2w-param}
%\end{figure}
%\begin{figure}
%	\centering
%	\includegraphics[width=0.5\textwidth]{fig/d2w_flops.pdf}
%	\caption{ }
%	\label{fig-d2w-flops}
%\end{figure}
Setting networks to change against depth, and networks to change against width. Comparing the performance of these two types of networks. We use hconv-64, with depth $\{38,50,62,74,98\}$ to show trend of the accuracy against \#params and flops. In the same time, We use a 20 layers of hconv-c with $c\in\{112,136,160,192,256\}$ to corresponding to the above models changing with depth, and show the trend of accuracy against \#params and flops. By comparing these two curve in figure \ref{fig-d2w-param} and figure \ref{fig-d2w-flops}, we can observe that a wider network has better performance comparing to a deeper network with the level parameters and flops. The reason we think is that, although skip connection structure can relieve the effect of gradient vanishing, it don't completely solve this problem, so that a deeper network still suffer a difficulty to update parameters when it's deep. Instead, a wider network don't increase the depth, so it don't suffer the problem of gradient vanishing. 
\subsection{Compare With Depthwise Separable Convolution}
\begin{table*}
	\centering
	\footnotesize
	\begin{center}
		\begin{tabular}{l|c|c|c|c|c|c|c|c|c|c}
			\hline
			\multirow{2}{*}{Depth}  & \multicolumn{5}{c|}{\#params($\times M$)} & \multicolumn{5}{c}{FLOPs($\times 10^8$)} \\	
			\cline{2-11}
			& Reg-16 & DW-35 & IGC-L24M2 & hconv-64 &hconv-f	& Reg-16 & DW-35 & IGC-L24M2 & hconv-64 &hconv-f \\
			\hline
			8 & 0.998 &0.056 & 0.046 &0.047  & 0.045 & 0.149 & 0.095 & 0.099 & 0.095 & 0.103\\
			20 &0.293 &0.168 & 0.151 &0.149  & 0.150 & 0.432 & 0.268 & 0.289 & 0.262 & 0.292\\
			38 & 0.584 &0.335 & 0.308 &0.302  & 0.308 & 0.856 &0.528 & 0.572 & 0.513 & 0.575\\
			50 & 0.777 &0.447 & 0.413 &0.404 & 0.413 & 1.140 &0.701 & 0.760 & 0.680 & 0.764\\		
			74& 1.164 & 0.670  & 0.623 &0.609 & 0.623 & 1.706  & 1.047 &  1.140 &1.014 & 1.142\\
			98& 1.551 & 0.894  & 0.832 &0.813 & 0.834 & 1.706  & 1.393 & 1.520  &1.348 & 1.520\\			
			\hline									
		\end{tabular}
	\end{center}
	\caption{Comparison of \#params and FLOPs}
	\label{dw-hconv-params-flops}
\end{table*}
\begin{table}
	\small
	\centering
	\begin{center}
		\begin{tabular}{l|c|c}
			\hline
			input size  & DW-c & hconv-c \\
			\hline			
			$32\times32$ &  ($3\times3$,c) &  ($3\times3$,c) \\
			\hline
			$32\times32$ & $\begin{bmatrix}
			3\times3,c \\ 
			3\times3,c
			\end{bmatrix} \times B$  & $\begin{bmatrix}
			3\times3,(L_1,K_1,c) \\ 
			3\times3,(L_1,K_1,c)
			\end{bmatrix} \times B$\\
			\hline
			$16\times16$ &  $\begin{bmatrix}
			3\times3,2c \\ 
			3\times3,2c
			\end{bmatrix} \times B$ & $\begin{bmatrix}
			3\times3,(L_2,K_2,2c) \\ 
			3\times3,(L_2,K_2,2c)
			\end{bmatrix} \times B$\\
			\hline		
			$8~\times~8$ &  $\begin{bmatrix}
			3\times3,4c \\ 
			3\times3,4c
			\end{bmatrix} \times B$ & $\begin{bmatrix}
			3\times3,(L_3,K_3,4c) \\ 
			3\times3,(L_3,K_3,4c)
			\end{bmatrix} \times B$\\
			\hline		
			$1\times1$ & \multicolumn{2}{c}{fc} \\
			\hline								
		\end{tabular}
	\end{center}
	\caption{Network structure.}
	\label{dw-hconv-structure}
\end{table}
\begin{table}
	\begin{center}
		\begin{tabular}{l|ccc}
			\hline
			& RegConv-16 & DW-35 & hconv-64 \\			
			\hline
			Depth & \multicolumn{3}{c}{Cifar10}\\
			\hline
			8 &- &$90.61\pm0.16$ & $91.00\pm0.33$\\
			20 &- &$93.47\pm0.1$ & $93.52\pm0.30$\\
			38 & - &$94.47\pm0.06$ & $94.26\pm0.08$\\
			\hline
			D & \multicolumn{3}{c}{Cifar100}\\
			\hline
			8 &$63.80\pm0.09$ &$67.00\pm0.29$ & $67.83\pm0.35$\\
			20 &$69.25\pm0.59$ &$70.97\pm0.10$ & $71.69\pm0.09$\\
			38 & $71.89\pm0.47$ &$73.68\pm0.54$ & $74.18\pm0.38$\\			
			\hline
			D & \multicolumn{3}{c}{tiny ImageNet}\\
			\hline
			8 &$44.37\pm0.62$ &$49.91\pm0.18$  & $51.40\pm0.32$\\
			20 & $51.80\pm0.33$ &$55.43\pm0.11$ & $56.01\pm0.47$\\
			38 & $54.14\pm0.28$ &$56.35\pm0.06$ & $56.90\pm0.41$\\
			\hline   		    			
		\end{tabular}
	\end{center}
	\caption{Comparison of depthwise and hconv. (Ident) }
	\label{table-compare-dw}
\end{table}
We use structure in table \ref{dw-hconv-structure}, to compare between depthwise separable convolution and hconv. We setting $K_1=8,K_2=16,K_3=32$, $L_1=L_2=L_3=2$,and use DW-35, hconv-64 as the network structure. The result is shown in table \ref{table-compare-dw}, where we compare on Cifar-100 and tiny Imagenet. Among all experiments in table \ref{table-compare-dw}, hconv has better performance than depthwise separable convolution, because of the wider network structure, although with similar or less parameters, as table \ref{dw-hconv-params-flops} showing. 
\subsection{Compare With IGC}
\begin{table}
	\begin{center}
		\begin{tabular}{l|ccc}
			\hline
			& Reg-16 & IGC & hconv \\			
			\hline
			Depth& \multicolumn{3}{c}{Cifar-10}\\
			\hline
			8 & $89.46\pm0.16$ & $90.31\pm 0.39$ & $90.93\pm0.24$\\%$90.91\pm0.30$\\
			20 & $92.24\pm0.17$ & $92.84\pm0.26$ & $93.22\pm0.13$\\%$93.28\pm0.15$\\
			38 & $90.77\pm0.23$ & $92.24\pm0.62$ & $91.52\pm0.10$\\%$91.90\pm0.42$\\
			\hline
			Depth & \multicolumn{3}{c}{Cifar-100}\\
			\hline
			8 & $62.83\pm0.32$ & $65.60\pm0.59$  & $67.72\pm0.68$\\%$67.62\pm0.40$\\
			20 & $67.90\pm0.14$ & $70.54\pm0.61$ & $71.59\pm0.02$\\%$71.21\pm0.20$\\
			38 & $64.04\pm0.42$ & $69.56\pm0.76$ & $69.62\pm0.79$\\%$70.15\pm0.40$\\	
			\hline   	
			Depth & \multicolumn{3}{c}{tiny Imagenet}\\
			\hline
			8 & - & $49.01\pm0.1$  & $51.04\pm1.58$\\
			20 & - & $53.82\pm0.39$ & $55.78\pm0.30$\\
			38 & - & - & -\\	
			\hline				    			
		\end{tabular}
	\end{center}
	\caption{Compare hconv with IGC on CIFAR-10 and CIFAR-100, on plain network structure. (plain)}
	\label{table-compare-igc-plain}
\end{table}
\begin{table*}
	\begin{center}
		\begin{tabular}{l|ccccc}
			\hline
			& Reg-16 & DW-16 & IGC & hconv-64 & hconv-f \\			
			\hline
			D & \multicolumn{5}{c}{Cifar-100}\\
			\hline
%			50 & $74.89\pm 0.67$ & $74.97\pm0.35$\\
%			74 & $75.41\pm0.75$ & $76.27\pm0.19$\\
%			98 & $76.15\pm0.50$ & $76.85\pm0.18$\\
			50 & $72.70\pm0.10$&$74.27\pm0.08$ & $74.89\pm 0.67$  & $74.76\pm0.41$ & -\\
			74 &$73.84\pm0.39$ & $75.34\pm0.09$ & $75.41\pm0.75$ & $75.3\pm0.12$ & -\\
			98 & - &$75.55\pm0.27$& $76.15\pm0.50$ &- &- \\
			\hline
			D & \multicolumn{5}{c}{tiny-imagenet}\\
			\hline
			50 & $55.55\pm0.11$ &$56.84\pm0.38$ & $57.80\pm0.22$ & $57.22\pm0.22$ & \\			
			74 & $56.22\pm0.11$ &$57.73\pm0.49$ & $58.59\pm0.16$ & $58.10\pm0.44$ & \\			
			98 & - &- & $58.93\pm0.18$ &-  &- \\
			\hline   		    			
		\end{tabular}
	\end{center}
	\caption{Compare hconv with IGC on tiny-imagenet and CIFAR-100, with skip connection. }
	\label{table-compare-igc-res}
\end{table*}
we designed network with hconv that has similar \#params of the network with IGC, and compare their performance. The result is shown in table \ref{table-compare-igc-plain} and table \ref{table-compare-igc-res}.

%{\small
%\bibliographystyle{ieee}
%\bibliography{egbib}
%}

\end{document}
